﻿/************************************************************************
* Copyright (c) 2020 All Rights Reserved.
*命名空间：esp8266api.Controllers
*文件名： EspController
*创建人： 覃乃林
*创建时间：2020/4/14 10:22:48
*描述
************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlueFox.Base.Mvc;
using BlueFox.Mqtt;


namespace esp8266api.Controllers
{
    [Route("api/[controller]")]
    public class EspController : PubController
    {
        private readonly MqttService _mqttService;
        public EspController(MqttService mqttService)
        {
            _mqttService = mqttService;
        }

        [HttpGet("Light")]
        public async Task<IActionResult> Light(int statu)
        {
            await _mqttService.ConnectAsync();
            await _mqttService.PublishAsync("light", statu == 0 ? "off" : "on");
            return Success();
        }
    }
}
