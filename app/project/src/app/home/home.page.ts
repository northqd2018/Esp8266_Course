import { Component } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private http: HttpClient) {}
  
  open(){
	  this.http.get(environment.host+`api/esp/light?statu=1`).subscribe(res=>{})
  }
  
  close(){
	  this.http.get(environment.host+`api/esp/light?statu=0`).subscribe(res=>{})
  }

}
